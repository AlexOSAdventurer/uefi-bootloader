.PHONY: qemu clean

all: gpt.img

CC_OPTIONS = -I/usr/include/efi -I/usr/include/efi/x86_64 -fpic -ffreestanding -fno-stack-protector -fno-stack-check -fshort-wchar -mno-red-zone -maccumulate-outgoing-args

%.o: %.c
	gcc $(CC_OPTIONS) -c $< -o $@

myapp.EFI: main.o
	ld -shared -Bsymbolic -L/usr/lib -T/usr/lib/elf_x86_64_efi.lds /usr/lib/crt0-efi-x86_64.o main.o -o main.so -lgnuefi -lefi
	objcopy -j .text -j .sdata -j .data -j .dynamic -j .dynsym  -j .rel -j .rela -j .rel.* -j .rela.* -j .reloc --target efi-app-x86_64 --subsystem=10 main.so myapp.EFI

BOOTX64.EFI: default.o
	ld -shared -Bsymbolic -L/usr/lib -T/usr/lib/elf_x86_64_efi.lds /usr/lib/crt0-efi-x86_64.o default.o -o default.so -lgnuefi -lefi
	objcopy -j .text -j .sdata -j .data -j .dynamic -j .dynsym  -j .rel -j .rela -j .rel.* -j .rela.* -j .reloc --target efi-app-x86_64 --subsystem=10 default.so BOOTX64.EFI

gpt.img:  myapp.EFI BOOTX64.EFI
	dd if=/dev/zero of=disk.img bs=1k count=55k
	parted --script disk.img \
		mklabel gpt \
		mkpart primary 1MiB 50MiB
	sudo losetup -P /dev/loop0 disk.img
	sudo mkfs.vfat /dev/loop0p1
	mkdir -p mount_folder
	sudo mount /dev/loop0p1 mount_folder
	sudo mkdir -p mount_folder/EFI/BOOT
	sudo cp myapp.EFI mount_folder/EFI/BOOT/myapp.EFI
	sudo cp BOOTX64.EFI mount_folder/EFI/BOOT/BOOTX64.EFI
	sudo cp test_file mount_folder/test_file
	sudo umount /dev/loop0p1
	sudo losetup -d /dev/loop0
	mv disk.img gpt.img

qemu: gpt.img
	qemu-system-x86_64 -L /usr/share/qemu -bios /usr/share/qemu/OVMF.fd -usb gpt.img

clean:
	rm -rf default.o default.so main.o main.so myapp.EFI BOOTX64.EFI gpt.img
	rm -rf mount_folder