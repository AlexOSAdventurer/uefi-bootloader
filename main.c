#include <efi.h>
#include <efilib.h>

EFI_HANDLE mainImageHandle;
EFI_GUID gEFI_LOADED_IMAGE_PROTOCOL_GUID = EFI_LOADED_IMAGE_PROTOCOL_GUID;
EFI_GUID gEFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
EFI_GUID gEFI_FILE_INFO_ID = EFI_FILE_INFO_ID;

#define uefi_call_wrapper_debug(function_name, ...) { print("Calling %s yields %d\n", L#function_name, uefi_call_wrapper(function_name, __VA_ARGS__); }


void disable_watchdog() {
	uefi_call_wrapper(BS->SetWatchdogTimer, 4, 0, 0, 0, NULL);
}

void resetConsole() {
	uefi_call_wrapper(ST->ConIn->Reset, 2, ST->ConIn, FALSE);
}

void waitForKey() {
	resetConsole();
	UINTN index;
	uefi_call_wrapper(BS->WaitForEvent, 3, 1, &(ST->ConIn->WaitForKey), &index);
}

EFI_INPUT_KEY readKeyNoCheck() {
	EFI_INPUT_KEY key;
	uefi_call_wrapper(ST->ConIn->ReadKeyStroke, 2, ST->ConIn, &key);
	resetConsole();
	return key;
}

EFI_INPUT_KEY readKey() {
	waitForKey();
	return readKeyNoCheck();
}

EFI_STATUS HandleProtocol(EFI_HANDLE handle, EFI_GUID* protocol, void** interface) {
	return uefi_call_wrapper(BS->OpenProtocol, 6, handle, protocol, interface, mainImageHandle, NULL, EFI_OPEN_PROTOCOL_BY_HANDLE_PROTOCOL);
}

void setup(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE* SystemTable) {
	InitializeLib(ImageHandle, SystemTable);
	disable_watchdog();
	mainImageHandle = ImageHandle;
}

EFI_FILE* openEFIVolume() {
	EFI_LOADED_IMAGE_PROTOCOL *LoadedImage;
	HandleProtocol(mainImageHandle, &gEFI_LOADED_IMAGE_PROTOCOL_GUID, (void**)&LoadedImage);

	EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *FileSystem;
	HandleProtocol(LoadedImage->DeviceHandle, &gEFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID, (void**)&FileSystem);

	EFI_FILE *root;
	if (uefi_call_wrapper(FileSystem->OpenVolume, 2, FileSystem, &root) == EFI_SUCCESS) {
		return root;
	}
	return NULL;
}

void* allocatePool(EFI_MEMORY_TYPE type, UINTN size) {
	void* data;
	if (uefi_call_wrapper(BS->AllocatePool, 3, type, size, &data) != EFI_SUCCESS) {
		Print(L"Allocation failed!\n");
	};
	return data;
}

void freePool(void* ptr) {
	if (uefi_call_wrapper(BS->FreePool, 1, ptr) != EFI_SUCCESS) { 
		Print(L"Freeing failed!\n");
	}
}

UINTN file_getInfoSize(EFI_FILE* file) {
	UINTN result = 0;
	if (uefi_call_wrapper(file->GetInfo, 4, file, &gEFI_FILE_INFO_ID, &result, NULL) != EFI_BUFFER_TOO_SMALL) {
		Print(L"Did not get info size\n");
	}
	return result;
}

EFI_FILE_INFO* file_getInfo(EFI_FILE* file) {
	UINTN fileInfoSize = file_getInfoSize(file);
	EFI_FILE_INFO *fileInfo = allocatePool(EfiLoaderData, fileInfoSize);
	if (uefi_call_wrapper(file->GetInfo, 4, file, &gEFI_FILE_INFO_ID, &fileInfoSize, fileInfo) == EFI_SUCCESS) {
		return fileInfo;
	}
	Print(L"Info not loaded properly!\n");
	return NULL;
}

void* readFile(EFI_FILE* file, UINTN fileSize) {
	void* data = allocatePool(EfiLoaderData, fileSize);
	if (uefi_call_wrapper(file->Read, 3, file, &fileSize, data) != EFI_SUCCESS) {
		Print(L"File loading failed!");
	}
	return data;
}

void* readAllFile(EFI_FILE* root, CHAR16* path, UINTN* size) {
	EFI_FILE* file;
	if (uefi_call_wrapper(root->Open, 5, root, &file, path, EFI_FILE_MODE_READ, EFI_FILE_READ_ONLY) == EFI_SUCCESS) {
		EFI_FILE_INFO *fileInfo = file_getInfo(file);
		*size = fileInfo->FileSize;
		freePool(fileInfo);
		return readFile(file, *size);
	}
	else { 
		Print(L"File opening failed!\n");
	}
	return NULL;
}

CHAR8* sanitizeData(CHAR8* data, UINTN size) {
	CHAR8* outputData = allocatePool(EfiLoaderData, size + 1);
	UINTN currentSize = 0;
	for (UINTN i = 0; i < size; i++) {
		if (data[i] != '\0') {
			outputData[currentSize] = data[i];
			currentSize++;
		}
	}
	outputData[currentSize] = '\0';
	return outputData;
}

EFI_STATUS EFIAPI efi_main(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE* SystemTable) {
	setup(ImageHandle, SystemTable);
	
	Print(L"Passed in character = %c\n", readKey().UnicodeChar);
	
	EFI_FILE* volume = openEFIVolume();
	Print(L"Volume opened\n");
	UINTN fileSize;
	CHAR8* data = readAllFile(volume, L"test_file", &fileSize);
	Print(L"File opened, size %d\n", fileSize);
	CHAR8* safeData = sanitizeData(data, fileSize);
	Print(L"File Contents:\n%a\n", safeData);
	freePool(data);
	return EFI_SUCCESS;
}
