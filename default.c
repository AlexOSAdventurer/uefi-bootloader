#include <efi.h>
#include <efilib.h>

void disable_watchdog() {
	uefi_call_wrapper(BS->SetWatchdogTimer, 4, 0, 0, 0, NULL);
}

EFI_STATUS EFIAPI efi_main(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE* SystemTable) {
	InitializeLib(ImageHandle, SystemTable);
	disable_watchdog();
	return EFI_SUCCESS;
}
